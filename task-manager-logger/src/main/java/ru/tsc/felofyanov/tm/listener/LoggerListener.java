package ru.tsc.felofyanov.tm.listener;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.ILoggerService;
import ru.tsc.felofyanov.tm.dto.EntityLogDTO;
import ru.tsc.felofyanov.tm.service.LoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import java.io.Serializable;

public class LoggerListener implements MessageListener {

    @NotNull
    final ILoggerService loggerService = new LoggerService();

    @Override
    @SneakyThrows
    public void onMessage(Message message) {
        if (!(message instanceof ObjectMessage)) return;
        @NotNull final Serializable serializable = ((ObjectMessage) message).getObject();
        if (serializable instanceof EntityLogDTO) loggerService.writeLog((EntityLogDTO) serializable);
    }
}
