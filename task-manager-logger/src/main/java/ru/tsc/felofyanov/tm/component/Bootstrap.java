package ru.tsc.felofyanov.tm.component;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.IReceiverService;
import ru.tsc.felofyanov.tm.listener.LoggerListener;
import ru.tsc.felofyanov.tm.service.ReceiverService;

public final class Bootstrap {

    @SneakyThrows
    public void run(@Nullable final String[] args) {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(ActiveMQConnectionFactory.DEFAULT_BROKER_URL);
        factory.setTrustAllPackages(true);
        @NotNull final IReceiverService receiverService = new ReceiverService(factory);
        receiverService.receive(new LoggerListener());
    }
}
