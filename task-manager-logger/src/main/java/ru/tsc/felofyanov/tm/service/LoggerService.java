package ru.tsc.felofyanov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.ILoggerService;
import ru.tsc.felofyanov.tm.dto.EntityLogDTO;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class LoggerService implements ILoggerService {

    @NotNull
    private static final String PROJECT_LOG_NAME = "logger/project.log";

    @NotNull
    private static final String TASK_LOG_NAME = "logger/task.log";

    @NotNull
    private static final String USER_LOG_NAME = "logger/user.log";

    @Override
    @SneakyThrows
    public void writeLog(@NotNull final EntityLogDTO message) {
        @Nullable final String className = message.getEntity();
        @Nullable final String fileName = getFileName(className);
        if (fileName == null) return;

        if (!Files.exists(Paths.get("logger")))
            Files.createDirectory(Paths.get("logger"));

        @NotNull final File file = new File(fileName);
        @NotNull final Path path = file.toPath();

        Files.deleteIfExists(path);
        Files.createFile(path);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file, true);
        @NotNull final String header = "Id: " + message.getId() + "; Type: " + message.getType() + "; Date: " + message.getDate() + "\n";
        fileOutputStream.write(header.getBytes());
        fileOutputStream.write(message.getEntity().getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    private String getFileName(@NotNull final String className) {
        switch (className) {
            case "ProjectDTO":
            case "Project":
                return PROJECT_LOG_NAME;
            case "TaskDTO":
            case "Task":
                return TASK_LOG_NAME;
            case "UserDTO":
            case "User":
                return USER_LOG_NAME;
            default:
                return null;
        }
    }
}

