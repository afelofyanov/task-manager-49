package ru.tsc.felofyanov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.dto.EntityLogDTO;

public interface ILoggerService {

    void writeLog(@NotNull EntityLogDTO message);
}
