package ru.tsc.felofyanov.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.service.ITokenService;

@Getter
@Setter
public class TokenService implements ITokenService {

    @Nullable
    private String token;
}
