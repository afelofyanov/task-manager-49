package ru.tsc.felofyanov.tm.command.user;

import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.endpoint.IUserEndpoint;
import ru.tsc.felofyanov.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    @Nullable
    public IUserEndpoint getUserService() {
        return null;
    }
}
