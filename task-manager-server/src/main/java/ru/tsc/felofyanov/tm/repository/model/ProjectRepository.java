package ru.tsc.felofyanov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.repository.model.IProjectRepository;
import ru.tsc.felofyanov.tm.model.Project;

import javax.persistence.EntityManager;

public class ProjectRepository extends AbstractUserOwnerRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull EntityManager entityManager) {
        super(entityManager, Project.class);
    }
}
