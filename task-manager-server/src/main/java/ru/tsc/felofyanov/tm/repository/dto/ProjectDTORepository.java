package ru.tsc.felofyanov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.repository.dto.IProjectDTORepository;
import ru.tsc.felofyanov.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;

public class ProjectDTORepository extends AbstractUserOwnerDTORepository<ProjectDTO> implements IProjectDTORepository {

    public ProjectDTORepository(@NotNull EntityManager entityManager) {
        super(entityManager, ProjectDTO.class);
    }
}
