package ru.tsc.afelofyanov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.felofyanov.tm.api.service.model.IProjectService;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.exception.entity.ModelEmptyException;
import ru.tsc.felofyanov.tm.exception.entity.ModelNotFoundException;
import ru.tsc.felofyanov.tm.exception.entity.UserNotFoundException;
import ru.tsc.felofyanov.tm.exception.field.*;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.service.model.ProjectService;

import java.util.List;

public class ProjectServiceTest extends AbstractTest {

    @NotNull
    private IProjectService projectService;

    @Before
    @Override
    public void init() {
        super.init();
        projectService = new ProjectService(CONNECTION_SERVICE);
    }

    @After
    @Override
    public void close() {
        super.close();
    }

    @Test
    public void add() {
        @NotNull Project project = new Project(testUser, "asdasd", "ddd");
        Assert.assertTrue(projectService.findAll().isEmpty());

        @Nullable Project projectNull = null;
        Assert.assertThrows(ModelEmptyException.class, () -> projectService.add(projectNull));

        projectService.add(project);
        Assert.assertFalse(projectService.findAll().isEmpty());
        Assert.assertNotNull(projectService.findOneById(project.getId()));
    }

    @Test
    public void updateById() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateById(null, "test", "test", "test"));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.updateById("test", null, "test", "test"));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateById("test", "test", null, "test"));
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.updateById("test", "test", "test", null));
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.updateById("test", "test", "test", "test"));

        @Nullable Project project = projectService.createByUserId(testUser.getId(), "DEMKA", "test");
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(projectService.findOneById(project.getId()));

        Assert.assertNotNull(projectService.updateById(testUser.getId(), project.getId(), "No demo", "tester"));
    }

    @Test
    public void updateByIndex() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateByIndex(null, 0, "test", "test"));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.updateByIndex("test", null, "test", "test"));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateByIndex("test", 0, null, "test"));
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.updateByIndex("test", 0, "test", null));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.updateByIndex("test", -1, "test", "test"));
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.updateByIndex(testUser.getId(), 1, "test", "test"));
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.updateByIndex(testUser.getId(), 0, "test", "test"));

        projectService.createByUserId(testUser.getId(), "DEMKA", "test");
        Assert.assertFalse(projectService.findAll().isEmpty());

        Assert.assertNotNull(projectService.updateByIndex(testUser.getId(), 0, "No demo", "tester"));
    }

    @Test
    public void changeStatusById() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeStatusById(null, "test", Status.NOT_STARTED));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.changeStatusById("test", null, Status.NOT_STARTED));
        Assert.assertThrows(StatusEmptyException.class, () -> projectService.changeStatusById("test", "test", null));
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.changeStatusById("test", "test", Status.NOT_STARTED));

        @NotNull Project project = projectService.createByUserId(testUser.getId(), "DEMKA", "test");
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(projectService.findOneById(project.getId()));

        Assert.assertNotNull(projectService.changeStatusById(testUser.getId(), project.getId(), Status.IN_PROGRESS));
    }

    @Test
    public void changeStatusByIndex() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeStatusByIndex(null, 0, Status.NOT_STARTED));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.changeStatusByIndex("test", null, Status.NOT_STARTED));
        Assert.assertThrows(StatusEmptyException.class, () -> projectService.changeStatusByIndex("test", 0, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.changeStatusByIndex("test", -1, Status.NOT_STARTED));
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.changeStatusByIndex("test", 1, Status.NOT_STARTED));
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.changeStatusByIndex("test", 0, Status.NOT_STARTED));

        projectService.createByUserId(testUser.getId(), "DEMKA", "test");
        Assert.assertFalse(projectService.findAll().isEmpty());

        Assert.assertNotNull(projectService.changeStatusByIndex(testUser.getId(), 0, Status.IN_PROGRESS));
    }

    @Test
    public void create() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        Assert.assertThrows(UserNotFoundException.class, () -> projectService.create(null, ""));
        Assert.assertThrows(UserNotFoundException.class, () -> projectService.create(null, "test"));
        Assert.assertThrows(UserNotFoundException.class, () -> projectService.create(null, null));

        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.createByUserId("", null));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.createByUserId("test", null));

        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.createByUserId("test", "test", null));

        projectService.createByUserId(testUser.getId(), "test");
        Assert.assertFalse(projectService.findAll().isEmpty());
        @Nullable List<Project> serviceTest = projectService.findAllByUserId("test1");
        Assert.assertEquals(0, serviceTest.size());

        serviceTest = projectService.findAllByUserId(testUser.getId());
        Assert.assertEquals(1, serviceTest.size());
    }
}
