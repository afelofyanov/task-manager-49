package ru.tsc.felofyanov.tm.service;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.service.ISenderService;
import ru.tsc.felofyanov.tm.dto.EntityLogDTO;

import javax.jms.*;

public final class SenderService implements ISenderService {

    @NotNull
    private final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);

    @Override
    @SneakyThrows
    public void send(@NotNull final EntityLogDTO entity) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic("TM_LOG");
        final MessageProducer producer = session.createProducer(destination);
        final ObjectMessage message = session.createObjectMessage(entity);
        producer.send(message);
        producer.close();
        session.close();
        connection.close();
    }

    @NotNull
    @Override
    @SneakyThrows
    public EntityLogDTO createMessage(@NotNull final Object object, @NotNull final String type) {
        @NotNull final EntityLogDTO message = new EntityLogDTO(type, object.getClass().getSimpleName());
        return message;
    }
}
