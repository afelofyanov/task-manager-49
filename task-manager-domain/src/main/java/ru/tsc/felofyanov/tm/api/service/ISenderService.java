package ru.tsc.felofyanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.dto.EntityLogDTO;

public interface ISenderService {

    void send(@NotNull EntityLogDTO entity);

    @NotNull
    EntityLogDTO createMessage(@NotNull Object object, @NotNull String type);
}
