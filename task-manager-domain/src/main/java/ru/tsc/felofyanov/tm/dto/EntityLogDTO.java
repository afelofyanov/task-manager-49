package ru.tsc.felofyanov.tm.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Getter
@XmlRootElement
public class EntityLogDTO implements Serializable {

    @NotNull
    private final String id = UUID.randomUUID().toString();

    @NotNull
    private final String type;

    @NotNull
    private final String entity;

    @NotNull
    private final String date = new Date().toString();

    public EntityLogDTO(@NotNull String type, @NotNull String entity) {
        this.type = type;
        this.entity = entity;
    }
}
